# Tapcodes

Tapcodes is an android application that helps you send tap codes.

## Getting Started

### Prerequisites

1. android 5.0+

### Installing

1. download and install the tapcodes.apk file

## Usage

You are able to click letters in a grid and get the sound of their tap codes.

## Built With

* [android studio](https://developer.android.com/studio) - Used as IDE

## Authors

* **Geographicallylazy** (https://gitlab.com/geographicallylazy)

## License

For now, just give me credit and share your changes with others, please.
