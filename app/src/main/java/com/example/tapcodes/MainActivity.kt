package com.example.tapcodes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.media.SoundPool
import android.os.Handler

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val letterTxList = arrayOf(
            arrayOf(
                findViewById<TextView>(R.id.letATx),
                findViewById(R.id.letBTx),
                findViewById(R.id.letCTx),
                findViewById(R.id.letDTx),
                findViewById(R.id.letETx)
            ),
            arrayOf(
                findViewById(R.id.letFTx),
                findViewById(R.id.letGTx),
                findViewById(R.id.letHTx),
                findViewById(R.id.letITx),
                findViewById(R.id.letJTx)
            ),
            arrayOf(
                findViewById(R.id.letLTx),
                findViewById(R.id.letMTx),
                findViewById(R.id.letNTx),
                findViewById(R.id.letOTx),
                findViewById(R.id.letPTx)
            ),
            arrayOf(
                findViewById(R.id.letQTx),
                findViewById(R.id.letRTx),
                findViewById(R.id.letSTx),
                findViewById(R.id.letTTx),
                findViewById(R.id.letUTx)
            ),
            arrayOf(
                findViewById(R.id.letVTx),
                findViewById(R.id.letWTx),
                findViewById(R.id.letXTx),
                findViewById(R.id.letYTx),
                findViewById(R.id.letZTx)
            )
        )

        val outTx = findViewById<TextView>(R.id.outTx)
        val clearTx = findViewById<TextView>(R.id.clearTx)
        val newlineTx = findViewById<TextView>(R.id.newlineTx)

        val letters = arrayOf(
            arrayOf(
                "A","B","C","D","E"
            ),
            arrayOf(
                "F","G","H","I","J"
            ),
            arrayOf(
                "L","M","N","O","P"
            ),
            arrayOf(
                "Q","R","S","T","U"
            ),
            arrayOf(
                "V","W","X","Y","Z"
            )
        )

        val handler = Handler()
        val soundPool = SoundPool.Builder().build()
        val tapSound = soundPool.load(this, R.raw.tap, 1)
        val r1 = Runnable{ soundPool.play(tapSound, 1f, 1f, 1, 0, 1f) }
        for (row in 1..letterTxList.size){
            for (let in 1..letterTxList[row-1].size){
                letterTxList[row-1][let-1].setOnClickListener{
                    handler.postDelayed({letterTxList[row-1][let-1].setBackgroundResource(R.drawable.ic_launcher_background)}, 10)

                    outTx.text = "${outTx.text}${letters[row-1][let-1]}"

                    var timeTrack = (10).toLong()

                    for (i in 1..row){

                        timeTrack += (600).toLong()
                        handler.postDelayed(r1, timeTrack)

                    }
                    timeTrack += (1200).toLong()

                    for (i in 1..let){

                        timeTrack += (600).toLong()
                        handler.postDelayed(r1, timeTrack)

                   }

                    timeTrack += (100).toLong()
                    handler.postDelayed({letterTxList[row-1][let-1].setBackgroundResource(R.drawable.black_square)}, timeTrack)
                }

            }
        }
        clearTx.setOnClickListener{
            outTx.text = ""
        }
        newlineTx.setOnClickListener{
            outTx.text = "${outTx.text}\n"
        }

    }
}
